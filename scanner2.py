#!/usr/bin/env python
import requests
from bs4 import BeautifulSoup
import re,json
from os.path import expanduser
from os.path import join
from os.path import exists
import telegram
import time
import hashlib

PROJECT_NAME = 'SHOP_SCANER'
FAKE_HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}

TEHNOSKARB_CATEGORY  = "igrovye-pristavki/c72"
URL_TEHNOSKARB 	  = "https://tehnoskarb.ua/{}?page={}"
LOGFILE_PATH   = join(expanduser("~"),"{}.json".format(PROJECT_NAME))
TEHNOSKARB_MAX_PAGES = None
SOUP 	  = BeautifulSoup(requests.get(URL_TEHNOSKARB.format(TEHNOSKARB_CATEGORY,1),headers=FAKE_HEADERS).text,'html.parser')
RESULTS   = {'tehnoskarb':[],'tehnostock':[]}

#CHECK MAX PAGES ONLY FOR TEHNOSKARB
try: 
	TEHNOSKARB_MAX_PAGES = int(SOUP.find('ul',class_='pages').find_all('li')[-1].text.strip())
except:
	TEHNOSKARB_MAX_PAGES = 1

# START PARSE SHOPS
def parsed_products():
	global RESULTS

	# parse tehnoskarb
	#############################################################################################

	for page in range(1,TEHNOSKARB_MAX_PAGES+1):
		page_url  = URL_TEHNOSKARB.format(TEHNOSKARB_CATEGORY,page)
		page_soup = BeautifulSoup(requests.get(page_url,headers=FAKE_HEADERS).text,'html.parser')
		products = page_soup.find('div',class_='products')
		items = list(filter(lambda x:x!=None, [i for i in products.find('ul').find_all('li')] ))

		for j in items:
			url = j.find('a')['href'].strip()
			try:
				offers = re.search(r'\d+',j.find('p').text.strip()).group(0)
			except:
				offers = 0
			if(url.split('/')[1] == 'catalog'):
				RESULTS['tehnoskarb'].append(
					{
					'offers': offers,
					"url" 	: "https://tehnoskarb.ua{}".format(url)}
					)
				# return { offers:int,url:str }
	#############################################################################################
	
	# parse tehnostock
	#############################################################################################

	tehnostok_url = 'https://technostock.com.ua/catalog/noutbuki-kompjutery-planshety/igrovye-konsoli?mode=list'
	soup = BeautifulSoup(requests.get(tehnostok_url).text,'html.parser')
	urls = [ j.find('a',href=True)['href'] for j in soup.find_all('div',class_='product-image-wrapper')]
	for i in urls:
		RESULTS['tehnostock'].append( { 'url':i,'offers':1 } )


	return RESULTS

#send to telegram
def send_message(t):
	TOKEN = "338343424:AAGfJxAaVtpUoQGDcnqRx32dZHQRGe4sFZ8"
	CHAT_ID = "266476630"
	CHAT_ID_Y = "371041145"
	bot = telegram.Bot(TOKEN)
	bot.send_message(CHAT_ID, text=t)
	bot.send_message(CHAT_ID_Y, text=t)

# products from file
def saved_products():
	with open(LOGFILE_PATH) as f:
		data = json.load(f)
	return data

def write_log():
	global RESULTS
	with open(LOGFILE_PATH, 'w') as outfile:
		json.dump(RESULTS, outfile,indent=4)
	RESULTS   = {'tehnoskarb':[],'tehnostock':[]}

#compare saved and parsed
def compare():
	global RESULTS

	if(exists(LOGFILE_PATH)):

		###
		parsed_items = parsed_products()
		saved_items = saved_products()

		for tehnoskarb_item in parsed_items['tehnoskarb']:
			if tehnoskarb_item not in saved_items['tehnoskarb']:
				if tehnoskarb_item['url'] not in list(map(lambda x: x['url'],saved_items['tehnoskarb'])):
					send_message( "новая позиция \n{}".format(tehnoskarb_item['url'])) # new item
				else:
					send_message( "новое предложение \n{}".format(tehnoskarb_item['url'])) # new offer

		###

		for tehnostock_item in parsed_items['tehnostock']:
			if tehnostock_item not in saved_items['tehnostock']:
				send_message( "новая позиция \n{}".format(tehnostock_item['url'])) # new item
				


				
		write_log()

	else:
		parsed_products()
		write_log()

while 1:
	compare()
	time.sleep(10)

#parsed_products()